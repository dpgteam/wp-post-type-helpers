<?php
namespace DPG\WpPostType;

/**
 * Product Post Type
 *
 * @package   Product_Post_Type
 */

/**
 * This class defines a custom metabox for adding product downloads.
 * 
 * @see https://github.com/WebDevStudios/CMB2/
 *
 * @package Product_Post_Type
 */
class PostType {
	const PREFIX = '_dpg-product_';
	static $post_type = '';
	static $supports = [];
	/**
	 * Registers the post type.
	 * @return [type] [description]
	 */
	public static function init() {
		add_action( 'init', array( get_called_class(), 'register' ) );
		
		// Add thumbnails to column view
		if ( post_type_supports( self::$post_type, 'thumbnail' ) ) {
			add_filter( 'manage_edit-' . self::$post_type . '_columns', array( get_called_class(), 'add_image_column'), 10, 1 );
			add_action( 'manage_' . self::$post_type . '_posts_custom_column', array( get_called_class(), 'display_image' ), 10, 1 );
		}
	}

	/**
	 * Post type registration.
	 * @return void
	 */
	public static function register() {} 
	
	/**
	 * Returns the specified custom field value.
	 * @param  string $custom_field
	 * @param  integer $post_ID
	 * @return mixed
	 */
	public static function getColumnData($custom_field, $post_ID) {
		// return get_post_meta( $post_ID, $custom_field, true );
	}

	/**
	 * Define labels for admin columns.
	 * @param  array $defaults
	 * @return array
	 */
	public static function columnHead($defaults) {

		// return $defaults;
	}

	/**
	 * Gets the custom field content.
	 * @param  string $custom_field
	 * @param  integer $post_ID
	 * @return mixed
	 */
	public static function columnContent() { 
		//$custom_field, $post_ID
		// return $custom_field;
	}

	/**
	 * Makes a column sortable.
	 * @param  array $columns
	 * @return 
	 */
    public static function columnSortable($columns) { }

	/**
	 * Add columns to post type list screen.
	 *
	 * @link http://wptheming.com/2010/07/column-edit-pages/
	 *
	 * @param array $columns Existing columns.
	 *
	 * @return array Amended columns.
	 */
	public function adminImageColumn( $columns ) {
		$column_thumbnail = array( 'thumbnail' => __( 'Image', 'product-post-type' ) );
		return array_slice( $columns, 0, 2, true ) + $column_thumbnail + array_slice( $columns, 1, null, true );
	}

	/**
	 * Custom column callback
	 *
	 * @global stdClass $post Post object.
	 *
	 * @param string $column Column ID.
	 */
	public function adminImageDisplay( $column ) {

		// global $post;
		switch ( $column ) {
			case 'thumbnail':
				// echo get_the_post_thumbnail( $post->ID, array(35, 35) );
				echo get_the_post_thumbnail( get_the_ID(), array( 35, 35 ) );
				break;
		}
	}

}
