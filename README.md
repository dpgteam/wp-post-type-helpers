# DPG WordPress Post Type Helpers
A collection of WordPress custom post types to interact with DPG Platform data.

##DPG/WpPostType/PostType

##DPG/WpPostType/Agent extends PostType

##DPG/WpPostType/Neighbourhood extends PostType

##DPG/WpPostType/Office extends PostType

##DPG/WpPostType/Property extends PostType